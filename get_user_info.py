import sys
import csv

def write_csv(name, email, phone):
    with open('users.csv', '+a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([name, email, phone])
        file.close()

def main():
    argv = sys.argv

    if len(argv) != 4:
        print("이름, 휴대폰번호, 이메일을 적어주세요")
        return

    write_csv(argv[1], argv[2], argv[3])

    print("Done")
    return

if __name__ == "__main__":
    main() 