import re
import csv

def main():
    with open('users.csv') as file:
        reader = list(csv.reader(file))
        for i,v in enumerate(reader):
            reader[i][1] = re.sub('[^0-9]', '-', v[1])
        
        writer = csv.writer(open('users.csv', 'w'))
        writer.writerows(reader)

        file.close()
    

if __name__ == "__main__":
    main()