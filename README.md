# Practice 2
This main purpose of this project is to practice git.
This project is saving user data (name, phone number, email) to csv file.
The project includes change phone number format, getting the domain from users' email address

## Documentation

### Installation
To install,
`$ git clone https://san8034@bitbucket.org/san8034/practice2.git`


### Usage
To add user data to csv file,
`$ python3 get_user_info.py <name> <phone number> <email>`

To change users' phone number format from the csv file,
`$ python3 clense_phonenum.py`

To get domain from users' email address,
`$ python3 get_domain.py`

### License
Practice 2 is Free software, and may be redistributed under the terms of specified in the [LICENSE](https://bitbucket.org/san8034/practice2/src/master/LICENSE.md) file.